package com.ripzery.mvp.data

/**
 * Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
 */


data class Photo(val name: String, var fullPath: String?)

data class Photos(val rootPath: String, val images: MutableList<Photo>)

data class Folder(var name: String, val cover: String? = null)

data class Folders(val folders: MutableList<Folder>)