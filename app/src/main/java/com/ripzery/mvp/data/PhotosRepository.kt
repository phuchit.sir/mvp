package com.ripzery.mvp.data

import rx.Observable

/**
* Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
*/

interface PhotosRepository {

    fun getPhotos(type: String, callback: (Photos) -> Unit, error: (Throwable) -> Unit)

    fun getPhoto(index: Int) : Photo

    fun getTypes(callback: (Folders) -> Unit, error: (Throwable) -> Unit)

    fun savePhoto(photo: Photo)

    fun refreshData()

}