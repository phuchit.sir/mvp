package com.ripzery.mvp.data

import com.ripzery.mvp.utils.ApiService
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.warn
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
 */
object PhotosRepositoryImpl : PhotosRepository, AnkoLogger {
    private val BASE_URL = "http://blog.ripzery.com:3000"
    private var mPhotos: Photos? = null
    private var mTypes: Folders? = null
    private var mObservableTypes: Observable<Folders> = Observable.just(mTypes)
    private var mObservablePhotos: Observable<Photos> = Observable.just(mPhotos)

    override fun getPhotos(type: String, callback: (Photos) -> Unit, error: (Throwable) -> Unit) {
        Observable.concat(mObservablePhotos, ApiService.apiService.getImages(type))
                .filter { it != null && it.rootPath.contains(type) }
                .first()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe({
                    setFullPath(it)
                    mPhotos = it
                    mObservablePhotos = Observable.just(mPhotos)
                    callback.invoke(it!!)
                }, { e ->
                    error.invoke(e)
                    warn { e }
                })
    }

    override fun getPhoto(index: Int): Photo {
        return mPhotos!!.images[index]
    }

    override fun getTypes(callback: (Folders) -> Unit, error: (Throwable) -> Unit) {
        Observable.concat(mObservableTypes, ApiService.apiService.getTypes())
                .filter { it != null }
                .first()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe({
                    mTypes = it
                    mObservableTypes = Observable.just(mTypes)
                    callback.invoke(it!!)
                }, { e ->
                    error.invoke(e)
                    warn { e }
                })
    }

    override fun savePhoto(photo: Photo) {
        throw UnsupportedOperationException()
    }

    override fun refreshData() {
        throw UnsupportedOperationException()
    }

    /** Method zone **/

    private fun setFullPath(it: Photos) {
        if (it.images[0].fullPath == null) it.images.forEach { photo -> photo.fullPath = BASE_URL + it.rootPath + photo.name }
    }

}