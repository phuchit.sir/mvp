package com.ripzery.mvp.screens.photodetail

import android.graphics.Bitmap
import android.support.v7.graphics.Palette
import com.ripzery.mvp.base.BaseContract
import com.ripzery.mvp.data.Photo

/**
* Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
*/

interface PhotoDetailContract {

    interface Presenter : BaseContract.LifecyclePresenter, BaseContract.ConnectivityPresenter {

        fun back()

        fun loadPhoto(index: Int)

        fun getSwatches(bitmap: Bitmap)

    }

    interface View : BaseContract.View {

        fun showSwatchView(swatch: MutableList<Palette.Swatch>)

        fun goBack()

        fun showPhoto(photo: Photo)

    }
}