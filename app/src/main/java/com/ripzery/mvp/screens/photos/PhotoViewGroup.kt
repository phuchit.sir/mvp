package com.ripzery.mvp.screens.photos

import android.annotation.TargetApi
import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ripzery.mvp.R
import com.ripzery.mvp.data.Photo
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.subjects.PublishSubject

class PhotoViewGroup : FrameLayout, AnkoLogger {

    /** Variable zone **/
    private var mViewContainer: View? = null
    private var mLayoutWidth = ""
    private var mLayoutHeight = ""
    private var mToolsSrc: Int = 0
    private var  measureSpec: Int = 0
    lateinit private var mIvPhoto: ImageView
    lateinit private var mModel: Photo
    lateinit private var mClickObservable: PublishSubject<Int>

    /** Override method zone **/
    constructor(context: Context) : super(context) {
        initInflate()
        initInstances()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, 0)
    }

    @TargetApi(21)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, defStyleRes)
    }


    private fun initInflate() {
        mViewContainer = inflate(context, R.layout.viewgroup_photo, this)
    }

    private fun initInstances() {
        // findViewById here
        if(!isInEditMode){
            mClickObservable = PublishSubject.create()
        }

        mIvPhoto = mViewContainer?.findViewById(R.id.ivPhoto) as ImageView

        mIvPhoto.setOnClickListener {
            mClickObservable.onNext(1)
        }
    }

    private fun initWithAttrs(attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) {
        mLayoutWidth = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_width")
        mLayoutHeight = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_height")
        mToolsSrc = attrs.getAttributeResourceValue("http://schemas.android.com/tools", "src", 0)

        if(isInEditMode){
           setPhotoEditMode()
        }
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        measureSpec = if(mLayoutHeight.equals("-1")) heightMeasureSpec else widthMeasureSpec

        super.onMeasure(measureSpec, measureSpec)
    }

    /** Method zone **/

    fun getClickObservable(): Observable<Int> {
        return mClickObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread())
    }

    fun getImageView(): ImageView {
        return mIvPhoto
    }

    fun setPhotoEditMode(){
        mIvPhoto.setImageDrawable(ContextCompat.getDrawable(context, mToolsSrc))
    }

    fun setModel(photo: Photo) {
        Glide.with(context).load(photo.fullPath).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().into(mIvPhoto)
    }
}
