package com.ripzery.mvp.screens.categories

import android.annotation.TargetApi
import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.ripzery.mvp.R
import com.ripzery.mvp.data.Folder
import org.jetbrains.anko.find

class CategoriesViewGroup : FrameLayout {

    /** Variable zone **/
    private val BASE_URL = "http://blog.ripzery.com:3000"
    lateinit private var mViewContainer: View
    lateinit private var mIvCategory: ImageView
    lateinit private var mTvTitle: TextView

    /** Override method zone **/
    constructor(context: Context) : super(context) {
        initInflate()
        initInstances()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, 0)
    }

    @TargetApi(21)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, defStyleRes)
    }


    private fun initInflate() {
        mViewContainer = inflate(context, R.layout.viewgroup_categories, this)
    }

    private fun initInstances() {
        // findViewById here
        mTvTitle = mViewContainer.find<TextView>(R.id.tvCategory)
        mIvCategory = mViewContainer.find<ImageView>(R.id.ivCategory)
    }

    private fun initWithAttrs(attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    /** Method zone **/

    fun setModel(folder: Folder) {
        mTvTitle.text = folder.name
        if (folder.cover != null) {
            mIvCategory.alpha = 1.0f
            mIvCategory.scaleType = ImageView.ScaleType.CENTER_CROP
            Glide.with(context).load(BASE_URL + folder.cover).into(mIvCategory)
        } else {
            mIvCategory.alpha = 0.54f
            mIvCategory.scaleType = ImageView.ScaleType.CENTER
            mIvCategory.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_photo_black_24dp))
        }
    }
}
