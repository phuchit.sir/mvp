package com.ripzery.mvp.screens.photos

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.ripzery.mvp.R
import com.ripzery.mvp.data.Photo
import com.ripzery.mvp.data.Photos
import com.ripzery.mvp.data.PhotosRepositoryImpl
import com.ripzery.mvp.screens.photodetail.PhotoDetailActivity
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.indeterminateProgressDialog

/**
 * Created by Euro (ripzery@gmail.com) on 3/10/16 AD.
 */
class PhotoFragment : Fragment(), PhotoContract.View {


    /** Variable zone **/
    private var mType: String? = null
    lateinit private var mPhotoPresenter: PhotoContract.Presenter
    private var mProgressBar: ProgressDialog? = null
    lateinit private var mPhotoAdapter: PhotoFragment.PhotoAdapter
    lateinit private var mLayoutNetworkErrorState: View
    lateinit private var mLayoutEmptyState: View
    lateinit private var mRecyclerView: RecyclerView
    private var mScrollPosition: Int = 0

    /** Static method zone **/
    companion object {
        val ARG_1 = "ARG_1"

        fun newInstance(param1: String?): PhotoFragment {
            val bundle: Bundle = Bundle()
            bundle.putString(ARG_1, param1)
            val photoFragment: PhotoFragment = PhotoFragment()
            photoFragment.arguments = bundle
            return photoFragment
        }

    }

    /** Lifecycle method zone **/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            /* if newly created */
            mType = arguments.getString(ARG_1)
        } else {
            mType = savedInstanceState.getString("type")
        }

        mPhotoPresenter = PhotoPresenter(PhotosRepositoryImpl, this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater!!.inflate(R.layout.fragment_photo, container, false)

        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()
    }

    override fun onStart() {
        super.onStart()
        mPhotoPresenter.handleStartActivity()
    }

    override fun onStop() {
        super.onStop()
        mPhotoPresenter.handleStopActivity()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            mScrollPosition = savedInstanceState.getInt("scroll_position")
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mScrollPosition = mRecyclerView.verticalScrollbarPosition
        outState?.putInt("scroll_position", mScrollPosition)
        outState?.putString("type", mType)
    }

    /** Override View Interface zone **/

    override fun showPhotoList(photos: Photos) {
        mPhotoAdapter.updateList(photos.images)
    }

    override fun showProgressBar() {
        mProgressBar = indeterminateProgressDialog("Please wait")
    }

    override fun hideProgressBar() {
        mProgressBar?.dismiss()
    }

    override fun showPhotoDetail(photo: Photo, index: Int) {
        val intent: Intent = Intent(activity, PhotoDetailActivity::class.java)
        intent.putExtra("photo_position", index)
        val clickedView: PhotoAdapter.PhotoViewHolder = mRecyclerView.findViewHolderForLayoutPosition(index) as PhotoAdapter.PhotoViewHolder
        val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, clickedView.getImageView() as View, "photo")
        startActivity(intent, options.toBundle())
    }

    override fun showNetworkError(enabled: Boolean) {
        mLayoutNetworkErrorState.visibility = if (enabled) View.VISIBLE else View.GONE
        mRecyclerView.visibility = if (enabled) View.GONE else View.VISIBLE
    }

    override fun showEmptyState() {
        mLayoutEmptyState.visibility = View.VISIBLE
    }

    /** Method zone **/

    private fun initInstance() {
        mRecyclerView = find<RecyclerView>(R.id.recyclerView)
        mLayoutNetworkErrorState = find<LinearLayout>(R.id.layoutNetworkErrorState)
        mLayoutEmptyState = find<LinearLayout>(R.id.layoutEmptyState)

        mPhotoAdapter = PhotoAdapter(arrayListOf(), mItemListener)
        mRecyclerView.layoutManager = GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)
        mRecyclerView.adapter = mPhotoAdapter
        mRecyclerView.scrollToPosition(mScrollPosition)

        mPhotoPresenter.loadPhotos(mType)
    }

    /** Inner class zone **/

    inner class PhotoAdapter(var list: MutableList<Photo>, var photoListener: ItemClickListener) : RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {
        override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
            holder.setModel(list[position])
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PhotoViewHolder {
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.viewholder_photo, parent, false)
            return PhotoViewHolder(view)
        }

        fun updateList(photoList: MutableList<Photo>) {
            list = photoList
            this.notifyDataSetChanged()
        }

        inner class PhotoViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

            lateinit private var mPhotoViewGroup: PhotoViewGroup

            init {
                mPhotoViewGroup = itemView?.findViewById(R.id.photoViewGroup) as PhotoViewGroup

                mPhotoViewGroup.getClickObservable().subscribe {
                    photoListener.photoClick(adapterPosition)
                }
            }

            fun setModel(photo: Photo) {
                mPhotoViewGroup.setModel(photo)
            }

            fun getImageView(): ImageView {
                return mPhotoViewGroup.getImageView()
            }

        }
    }

    /** Interface zone **/

    interface ItemClickListener {
        fun photoClick(index: Int)
    }

    /** Listener zone **/

    private var mItemListener = object : ItemClickListener {
        override fun photoClick(index: Int) {
            mPhotoPresenter.openPhotoDetail(index)
        }
    }

}