package com.ripzery.mvp.screens.photodetail

import android.annotation.TargetApi
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.ripzery.mvp.R
import org.jetbrains.anko.find

class SwatchViewGroup : FrameLayout {

    /** Variable zone **/
    lateinit private var mViewContainer: View
    lateinit private var mVSwatch: View
    lateinit private var mTvSwatch: TextView

    /** Override method zone **/
    constructor(context: Context) : super(context) {
        initInflate()
        initInstances()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, 0)
    }

    @TargetApi(21)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, defStyleRes)
    }


    private fun initInflate() {
        mViewContainer = inflate(context, R.layout.viewgroup_swatch, this)
    }

    private fun initInstances() {
        // findViewById here
        mVSwatch = mViewContainer.find(R.id.vSwatch)
        mTvSwatch = mViewContainer.find(R.id.tvSwatch)
    }

    private fun initWithAttrs(attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(heightMeasureSpec, heightMeasureSpec)
    }

    /** Method zone **/

    fun setModel(viewColor: Int, textColor: Int, text: String) {
        mTvSwatch.setTextColor(textColor)
        mVSwatch.setBackgroundColor(viewColor)
        mTvSwatch.text = text
    }
}
