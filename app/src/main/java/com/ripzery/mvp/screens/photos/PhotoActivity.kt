package com.ripzery.mvp.screens.photos

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ripzery.mvp.R
import com.ripzery.mvp.extensions.replaceFragment

class PhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        if(savedInstanceState == null){
            val type = intent.getStringExtra("type")
            replaceFragment(fragment = PhotoFragment.newInstance(type))
        }
    }
}
