package com.ripzery.mvp.screens.categories

import com.ripzery.mvp.base.BaseContract
import com.ripzery.mvp.data.Folder
import com.ripzery.mvp.data.Folders

/**
 * Created by Euro (ripzery@gmail.com) on 6/30/2016 AD.
 */
interface CategoriesContract {

    interface View : BaseContract.View{

        fun showCategories(folders: Folders)

        fun showPhotoList(type: String? = null)

        fun showFirebasePhotoPath(path: String)

        fun showCreateCategoryDialog()

        fun addCategory(folder: Folder)

    }

    interface Presenter : BaseContract.LifecyclePresenter, BaseContract.ConnectivityPresenter {

        fun loadCategories()

        fun loadPhotoList(index: Int)

        fun fabClicked()

        fun createCategory(title: String)

    }
}