package com.ripzery.mvp.screens.photos

import com.ripzery.mvp.base.BaseContract
import com.ripzery.mvp.data.Photo
import com.ripzery.mvp.data.Photos

/**
* Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
*/

interface PhotoContract {

    interface Presenter : BaseContract.LifecyclePresenter, BaseContract.ConnectivityPresenter {

        fun loadPhotos(type: String?)

        fun openPhotoDetail(index: Int)

    }

    interface View : BaseContract.View {

        fun showPhotoList(photos: Photos)

        fun showPhotoDetail(photo: Photo, index: Int)

        fun showEmptyState()

    }

}