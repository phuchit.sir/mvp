package com.ripzery.mvp.screens.categories

import android.util.MalformedJsonException
import com.ripzery.mvp.data.Folder
import com.ripzery.mvp.data.PhotosRepository
import com.ripzery.mvp.modules.ConnectivityModule
import org.jetbrains.anko.AnkoLogger
import java.net.UnknownHostException

/**
 * Created by Euro (ripzery@gmail.com) on 6/30/2016 AD.
 */
class CategoriesPresenter(val photosRepository: PhotosRepository, val view: CategoriesContract.View) : AnkoLogger, CategoriesContract.Presenter {
    lateinit private var mConnectivity: ConnectivityModule

    init {
        mConnectivity = ConnectivityModule(this)
        mConnectivity.subscribe()
    }

    /** Override UserActionsListener Interface zone **/

    override fun loadPhotoList(index: Int) {
        photosRepository.getTypes({
            if(it.folders[index].cover == null){
                view.showPhotoList(null)
            }else{
                view.showPhotoList(it.folders[index].name)
            }
        }, {
            if (it.cause is MalformedJsonException) {
                // empty repo

                view.showPhotoList(null)

            } else if (it.cause is UnknownHostException) {
                // internet error
                handleOffline()
            }
        })
    }

    override fun loadCategories() {
        view.showProgressBar()
        photosRepository.getTypes({
            view.showCategories(it)
            view.hideProgressBar()
        }, {
            view.hideProgressBar()
            handleOffline()
        })
    }

    override fun handleOffline() {
        view.showNetworkError(true)
    }

    override fun handleOnline() {
        view.showNetworkError(false)
        loadCategories()
    }

    override fun handleStartActivity() {
        mConnectivity.subscribe()
    }

    override fun handleStopActivity() {
        mConnectivity.unsubscribe()
    }

    override fun fabClicked() {
        view.showCreateCategoryDialog()
    }

    override fun createCategory(title: String) {
        view.addCategory(Folder(title))
    }

}