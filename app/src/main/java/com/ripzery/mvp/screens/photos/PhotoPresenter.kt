package com.ripzery.mvp.screens.photos

import android.os.Handler
import com.ripzery.mvp.data.PhotosRepository
import com.ripzery.mvp.modules.ConnectivityModule
import org.jetbrains.anko.AnkoLogger
import java.net.UnknownHostException

/**
 * Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
 */
class PhotoPresenter(val photosRepository: PhotosRepository, val view: PhotoContract.View) : PhotoContract.Presenter, AnkoLogger {

    lateinit private var mConnectivity: ConnectivityModule
    private val LOADING_DELAY: Long = 700
    private var mPhotoType: String = ""
    private val mLoading = Handler()

    init {
        mConnectivity = ConnectivityModule(this)
        mConnectivity.subscribe()
    }

    /** Override UserActionsListener Interface zone **/

    override fun loadPhotos(type: String?) {

        if (type != null) {
            mPhotoType = type
            view.showProgressBar()
            photosRepository.getPhotos(type, {
                view.showPhotoList(it)
                view.hideProgressBar()
            }, {
                if (it.cause is UnknownHostException) {
                    view.hideProgressBar()
                    handleOffline()
                }
            })
        } else {
            view.showEmptyState()
        }

    }

    override fun handleOffline() {
        view.showNetworkError(true)
    }

    override fun handleOnline() {
        view.showNetworkError(false)
        loadPhotos(mPhotoType)
    }

    override fun handleStartActivity() {
        mConnectivity.subscribe()
    }

    override fun handleStopActivity() {
        mConnectivity.unsubscribe()
        view.hideProgressBar()
        mLoading.removeCallbacks(mLoadingRunnable)
    }

    override fun openPhotoDetail(index: Int) {
        mLoading.postDelayed(mLoadingRunnable, LOADING_DELAY)
        view.showPhotoDetail(photosRepository.getPhoto(index), index)
    }

    /** Listener zone **/
    private var mLoadingRunnable: Runnable = Runnable {
        view.showProgressBar()
    }
}
