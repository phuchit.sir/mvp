package com.ripzery.mvp.screens.photodetail

import android.graphics.Bitmap
import android.support.v7.graphics.Palette
import com.ripzery.mvp.data.PhotosRepository
import io.sweers.rxpalette.asObservable
import kotlinx.android.synthetic.main.viewgroup_photo.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.warn
import rx.Subscription

/**
 * Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
 */

class PhotoDetailPresenter(var photosRepository: PhotosRepository, var view: PhotoDetailContract.View) : PhotoDetailContract.Presenter,
        AnkoLogger {

    private var mPaletteSubscription:Subscription? = null

    /** Override UserActionsListener Interface zone **/

    override fun getSwatches(bitmap: Bitmap) {
        mPaletteSubscription = Palette.Builder(bitmap).maximumColorCount(16).asObservable().subscribe ({ palette ->
            view.showSwatchView(palette.swatches)
        }, { error ->
            warn (error)
        })
    }

    override fun handleOffline() {
        view.showNetworkError(true)
    }

    override fun handleOnline() {
        view.showNetworkError(false)
    }

    override fun handleStopActivity() {
        mPaletteSubscription?.unsubscribe()
    }

    override fun handleStartActivity() {
        // Nothing to do now..
    }

    override fun loadPhoto(index: Int) {
        view.showPhoto(photosRepository.getPhoto(index))
    }

    override fun back() {
        view.goBack()
    }



}