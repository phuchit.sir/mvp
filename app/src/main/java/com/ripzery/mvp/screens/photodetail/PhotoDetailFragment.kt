package com.ripzery.mvp.screens.photodetail

import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.graphics.Palette
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ripzery.mvp.R
import com.ripzery.mvp.data.Photo
import com.ripzery.mvp.data.PhotosRepositoryImpl
import com.ripzery.mvp.utils.Contextor
import kotlinx.android.synthetic.main.fragment_photo_detail.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.find
import org.jetbrains.anko.info
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.warn

/**
 * Created by Euro (ripzery@gmail.com) on 3/10/16 AD.
 */
class PhotoDetailFragment : Fragment(), PhotoDetailContract.View, AnkoLogger {

    /** Variable zone **/
    private var mPosition: Int = 0
    lateinit private var mIvPhoto: ImageView
    lateinit private var mPresenter: PhotoDetailContract.Presenter
    lateinit private var mSwatchAdapter: SwatchesAdapter
    private var mProgressBar: ProgressDialog? = null

    /** Static method zone **/
    companion object {
        val ARG_1 = "ARG_1"

        fun newInstance(position: Int): PhotoDetailFragment {
            val bundle: Bundle = Bundle()
            bundle.putInt(ARG_1, position)
            val photoDetailFragment: PhotoDetailFragment = PhotoDetailFragment()
            photoDetailFragment.arguments = bundle
            return photoDetailFragment
        }

    }

    /** Activity method zone  **/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            /* if newly created */
            mPosition = arguments.getInt(ARG_1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater!!.inflate(R.layout.fragment_photo_detail, container, false)

        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()
    }

    override fun onStart() {
        super.onStart()
        mPresenter.handleStartActivity()
    }

    override fun onStop() {
        super.onStop()
        mPresenter.handleStopActivity()
    }

    /** Override View Interface zone **/

    override fun showNetworkError(enabled: Boolean) {
        info { enabled }
    }

    override fun goBack() {
        activity.supportFinishAfterTransition()
    }

    override fun showPhoto(photo: Photo) {
        Glide.with(Contextor.context)
                .load(photo.fullPath)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .listener(object : RequestListener<String, Bitmap> {
                    override fun onResourceReady(resource: Bitmap, model: String?, target: Target<Bitmap>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                        try {
                            mPresenter.getSwatches(resource)
                            mIvPhoto.setImageBitmap(resource)
                            activity.supportStartPostponedEnterTransition()
                        } catch (e: NullPointerException) {
                            warn { e }
                        }

                        return true
                    }

                    override fun onException(e: Exception?, model: String?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        warn { e }
                        activity.supportStartPostponedEnterTransition()
                        return true
                    }

                })
                .into(mIvPhoto)


    }

    override fun showProgressBar() {
        mProgressBar = indeterminateProgressDialog("Please wait")
    }

    override fun hideProgressBar() {
        mProgressBar?.dismiss()
    }

    /** Method zone **/

    private fun initInstance() {
        mIvPhoto = find(R.id.ivPhoto)

        mSwatchAdapter = SwatchesAdapter(arrayListOf())

        recyclerView.adapter = mSwatchAdapter
        recyclerView.layoutManager = GridLayoutManager(context, 2, GridLayoutManager.HORIZONTAL, false)

        mPresenter = PhotoDetailPresenter(PhotosRepositoryImpl, this)
        mPresenter.loadPhoto(mPosition)

    }

    override fun showSwatchView(swatches: MutableList<Palette.Swatch>) {
        mSwatchAdapter.updateList(swatches)
    }

    /** Inner class zone **/
    inner class SwatchesAdapter(var list: MutableList<Palette.Swatch>) : RecyclerView.Adapter<SwatchesAdapter.SwatchViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SwatchViewHolder {
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.viewholder_swatch, parent, false)
            return SwatchViewHolder(view)
        }

        override fun onBindViewHolder(holder: SwatchViewHolder, position: Int) {
            holder.setModel(list[position].rgb, list[position].titleTextColor, (position + 1).toString())
        }

        override fun getItemCount(): Int {
            return list.size
        }

        fun updateList(swatchesList: MutableList<Palette.Swatch>) {
            this.list = swatchesList
            notifyDataSetChanged()
        }

        inner class SwatchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            lateinit private var mSwatchViewGroup: SwatchViewGroup

            init {
                mSwatchViewGroup = itemView.find(R.id.swatchViewGroup)
            }

            fun setModel(viewColor: Int, textColor: Int, text: String) {
                mSwatchViewGroup.setModel(viewColor, textColor, text)
            }
        }
    }
}