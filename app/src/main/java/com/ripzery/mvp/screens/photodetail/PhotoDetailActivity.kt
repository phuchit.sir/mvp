package com.ripzery.mvp.screens.photodetail

import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.ripzery.mvp.R
import com.ripzery.mvp.extensions.replaceFragment
import kotlinx.android.synthetic.main.activity_photo_detail.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.configuration

class PhotoDetailActivity : AppCompatActivity(), AnkoLogger {


    /** Variable zone **/
    private var mPhotoPosition: Int = 0

    /** Lifecycle zone **/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_detail)
        supportPostponeEnterTransition()
        initInstance()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition()
            }
        }
        return true
    }

    override fun onBackPressed() {
        supportFinishAfterTransition()
    }

    private fun initInstance() {
        setupToolbar()
        mPhotoPosition = intent.getIntExtra("photo_position", 0)
        replaceFragment(fragment = PhotoDetailFragment.newInstance(mPhotoPosition))
    }

    private fun setupToolbar() {
        if(configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            toolbar.visibility = View.VISIBLE
            setSupportActionBar(toolbar)
            supportActionBar?.title = "Detail"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(true)
        }else{
            toolbar.visibility = View.GONE
        }
    }

}
