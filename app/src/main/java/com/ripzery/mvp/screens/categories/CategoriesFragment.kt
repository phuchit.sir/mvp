package com.ripzery.mvp.screens.categories

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ripzery.mvp.R
import com.ripzery.mvp.data.Folder
import com.ripzery.mvp.data.Folders
import com.ripzery.mvp.data.PhotosRepositoryImpl
import com.ripzery.mvp.screens.photos.PhotoActivity
import com.ripzery.mvp.utils.DialogUtils
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.find
import org.jetbrains.anko.info
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.toast

/**
 * Created by Euro (ripzery@gmail.com) on 3/10/16 AD.
 */
class CategoriesFragment : Fragment(), CategoriesContract.View, AnkoLogger {
    /** Variable zone **/
    lateinit private var mParam1: String
    lateinit private var mCategoriesPresenter: CategoriesContract.Presenter
    lateinit private var mLayoutEmpty: View
    lateinit private var mFab: FloatingActionButton
    lateinit private var mRecyclerView: RecyclerView
    private var mCategoriesAdapter: CategoriesAdapter? = null
    private var mProgressDialog: ProgressDialog? = null
    private var mScrollPosition: Int = 0


    /** Static method zone **/
    companion object {
        val ARG_1 = "ARG_1"

        fun newInstance(param1: String): CategoriesFragment {
            var bundle: Bundle = Bundle()
            bundle.putString(ARG_1, param1)
            val categoriesFragment: CategoriesFragment = CategoriesFragment()
            categoriesFragment.arguments = bundle
            return categoriesFragment
        }

    }

    /** Activity method zone  **/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            /* if newly created */
            mParam1 = arguments.getString(ARG_1)

            retainInstance = true

        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: View = inflater!!.inflate(R.layout.fragment_categories, container, false)
        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewInstance()
    }

    override fun onStart() {
        super.onStart()
        mCategoriesPresenter.handleStartActivity()
    }

    override fun onStop() {
        super.onStop()
        mCategoriesPresenter.handleStopActivity()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            mScrollPosition = savedInstanceState.getInt("scroll_position")
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mScrollPosition = mRecyclerView.verticalScrollbarPosition
        outState?.putInt("scroll_position", mScrollPosition)
    }

    /** Override View Interface zone **/

    override fun showProgressBar() {
        mProgressDialog = indeterminateProgressDialog("Please wait")
    }

    override fun hideProgressBar() {
        mProgressDialog?.dismiss()
    }

    override fun showCategories(folders: Folders) {
        mCategoriesAdapter?.updateList(folders)
    }

    override fun showNetworkError(enabled: Boolean) {
        mLayoutEmpty.visibility = if (enabled) View.VISIBLE else View.GONE
        mRecyclerView.visibility = if (enabled) View.GONE else View.VISIBLE
    }

    override fun showPhotoList(type: String?) {
        val intent: Intent = Intent(activity, PhotoActivity::class.java)
        intent.putExtra("type", type)
        startActivity(intent)
    }

    override fun showCreateCategoryDialog() {
        DialogUtils.getCreateCategoryDialog(context) {
            mCategoriesPresenter.createCategory(it)
        }.show()
    }

    override fun addCategory(folder: Folder) {
        mCategoriesAdapter?.addFolder(folder)
        mRecyclerView.smoothScrollToPosition(mCategoriesAdapter!!.itemCount - 1)
    }

    /** Method zone **/

    private fun initViewInstance() {
        mRecyclerView = find(R.id.recyclerView)
        mLayoutEmpty = find(R.id.layoutNetworkErrorState)
        mFab = find(R.id.fab)

        mCategoriesPresenter = CategoriesPresenter(PhotosRepositoryImpl, this)
        mCategoriesPresenter.loadCategories()

        mCategoriesAdapter = CategoriesAdapter(Folders(arrayListOf()), mItemClickListener)
        mRecyclerView.layoutManager = LinearLayoutManager(activity)
        mRecyclerView.adapter = mCategoriesAdapter
        mRecyclerView.scrollToPosition(mScrollPosition)

        mFab.setOnClickListener {
            mCategoriesPresenter.fabClicked()
        }
    }

    /** Inner class zone **/

    inner class CategoriesAdapter(var categoriesList: Folders, var listener: ItemClickListener) : RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>() {
        override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
            holder.setModel(categoriesList.folders[position])

            holder.categoriesViewGroup.setOnClickListener { listener.onItemClicked(position) }
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CategoriesViewHolder {
            val view = LayoutInflater.from(parent?.context).inflate(R.layout.viewholder_categories, parent, false)
            return CategoriesViewHolder(view)
        }

        override fun getItemCount(): Int {
            return categoriesList.folders.size
        }

        fun updateList(categoriesList: Folders) {
            this.categoriesList = categoriesList
            notifyDataSetChanged()
        }

        fun addFolder(folder: Folder){
            this.categoriesList.folders.add(folder)
            notifyDataSetChanged()
        }

        inner class CategoriesViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
            lateinit var categoriesViewGroup: CategoriesViewGroup

            init {
                categoriesViewGroup = itemView!!.find<CategoriesViewGroup>(R.id.categoriesViewGroup)

            }

            fun setModel(folder: Folder) {
                categoriesViewGroup.setModel(folder)
            }
        }
    }

    override fun showFirebasePhotoPath(path: String) {
        toast(path)
    }

    /** Listener zone **/

    private var mItemClickListener = object : ItemClickListener {
        override fun onItemClicked(index: Int) {
            mCategoriesPresenter.loadPhotoList(index)
        }
    }

    /** Interface zone **/

    interface ItemClickListener {
        fun onItemClicked(index: Int)
    }
}