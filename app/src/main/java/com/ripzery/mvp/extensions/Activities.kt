package com.ripzery.mvp.extensions

import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ripzery.mvp.R

/**
 * Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
 */

fun AppCompatActivity.replaceFragment(layout: Int = R.id.contentContainer, fragment: Fragment){
    supportFragmentManager.beginTransaction().replace(layout, fragment).commit()
}

fun AppCompatActivity.replaceSharedElementFragment(layout: Int = R.id.contentContainer, fragment: Fragment, view: View, transitionName: String){
    supportFragmentManager.beginTransaction().replace(layout, fragment).addSharedElement(view, transitionName).commit()
}