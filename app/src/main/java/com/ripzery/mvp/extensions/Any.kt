package com.ripzery.mvp.extensions

import android.annotation.TargetApi
import android.os.Build
import com.ripzery.mvp.BuildConfig

/**
 * Created by Euro (ripzery@gmail.com) on 6/29/2016 AD.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
fun Any.supportsLollipop(code:() -> Unit) {
    if(BuildConfig.VERSION_CODE >= Build.VERSION_CODES.LOLLIPOP){
        code()
    }
}