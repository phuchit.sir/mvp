package com.ripzery.mvp

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.ripzery.mvp.utils.Contextor
import io.fabric.sdk.android.Fabric

/**
 * Created by Euro (ripzery@gmail.com) on 6/29/2016 AD.
 */

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Contextor.context = this
        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())
    }
}