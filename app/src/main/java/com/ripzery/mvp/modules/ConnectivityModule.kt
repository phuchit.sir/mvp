package com.ripzery.mvp.modules

import com.github.pwittchen.reactivenetwork.library.ConnectivityStatus
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork
import com.ripzery.mvp.base.BaseContract
import com.ripzery.mvp.base.BaseModuleContract
import com.ripzery.mvp.utils.Contextor
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
* Created by Euro (ripzery@gmail.com) on 6/30/2016 AD.
*/

class ConnectivityModule(var presenter: BaseContract.ConnectivityPresenter) : BaseModuleContract.Base , AnkoLogger {
    private var mObserveNetwork: Subscription? = null

    override fun subscribe() {
        if(mObserveNetwork == null || mObserveNetwork!!.isUnsubscribed) {
            mObserveNetwork = ReactiveNetwork().observeNetworkConnectivity(Contextor.context)
                    .skip(1) // we don't have to get connectivity state immediately, but want to keep listening instead
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe {
                        info { it }
                        if (it.equals(ConnectivityStatus.OFFLINE)) {
                            // This module don't care when offline. Retrofit will responsible instead.
//                            presenter.handleOffline()
                        } else {
                            presenter.handleOnline()
                        }
                    }
        }
    }

    override fun unsubscribe() {
        mObserveNetwork?.unsubscribe()
    }

}