package com.ripzery.mvp.base

import android.graphics.Bitmap

/**
 * Created by Euro (ripzery@gmail.com) on 6/30/2016 AD.
 */

interface BaseModuleContract {

    interface Base {
        fun subscribe()

        fun unsubscribe()
    }

    interface FirebaseStorage : BaseModuleContract.Base {

        fun getFolderPath(name: String, code: (url: String) -> Unit)

        fun upload(bitmap: Bitmap)

    }

}