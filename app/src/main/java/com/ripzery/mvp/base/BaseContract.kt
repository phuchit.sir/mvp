package com.ripzery.mvp.base

/**
 * Created by Euro (ripzery@gmail.com) on 6/30/2016 AD.
 */
interface BaseContract {

    interface View {

        fun showNetworkError(enabled: Boolean)

        fun showProgressBar()

        fun hideProgressBar()

    }

    interface LifecyclePresenter {

        fun handleStopActivity()

        fun handleStartActivity()

    }

    interface ConnectivityPresenter {

        fun handleOffline()

        fun handleOnline()

    }


}