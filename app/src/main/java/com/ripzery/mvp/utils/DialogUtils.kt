package com.ripzery.mvp.utils

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog

/**
 * Created by Euro (ripzery@gmail.com) on 7/5/2016 AD.
 */

object DialogUtils {
    fun getCreateCategoryDialog(context: Context, code: (msg: String) -> Unit): MaterialDialog {
        return MaterialDialog.Builder(context)
                .title("Create account")
                .content("Title")
                .input("Category title", null, false) { dialog, input ->
                    code(input.toString())
                }
                .positiveText("OK")
                .negativeText("Cancel")
                .build()

    }
}