package com.ripzery.mvp.utils

import com.ripzery.mvp.data.Folders
import com.ripzery.mvp.data.Photos
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable

/**
 * Created by Euro (ripzery@gmail.com) on 6/28/2016 AD.
 */

object ApiService {

    val apiService = buildRetrofit().create(ApiEndpoint::class.java)

    fun buildRetrofit(): Retrofit {

        return Retrofit.Builder()
                .baseUrl("http://blog.ripzery.com:3000/api2/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

    }
}

interface ApiEndpoint {
    @GET("images")
    fun getImages(@Query("type") type: String): Observable<Photos>

    @POST("getTypes")
    fun getTypes() : Observable<Folders>
}